#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266TrueRandom.h>
#include <EEPROM.h>


const char* device_name = "dimmer";   // This device is dimmer circuit for lamp.
int upload_interval = 1000;       // Uploading current brightness level and brightness level by potentiometer in miliseconds.

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";   // Server has been built on the router(RPi 3) itself

const char* relay_set_suffix = "/relay_set";       // relay setting for AC220V
const char* relay_state_suffix = "/relay_state";    // relay state

const char* brightness_level_set_suffix = "/brightness_level/set";   // brightness level of lamp (RPI side)
const char* brightness_poten_level_suffix = "/brightness_poten/level";   // brightness level by potentiometer (Node side)
const char* curr_brightness_level_suffix = "/curr_brightness/level"; // current brightness level

char* buf_brightness_poten_level = new char[10];
char* buf_curr_brightness_level = new char[10];

// define pin for relay, power_led, pwm_out
int PIN_relay_switch = 5;  // use D1, GPIO5
int PIN_powerOn_led = 4;   // use D2, GPIO4
int PIN_pwm_out = 14;  // use D5, GPIIO14
#define AnalogPin A0 // define analog pin
//int AnalogPin = 0;

// Global variables
char* val;  //val is payload value for topic of brightness setting
int brightness, brightness_poten, brightness_poten_level;
int curr_brightness_level; //current brightness level in digit type
//char* buf_curr_brightness_level; //current brightness level in string type

WiFiClient espClient;
PubSubClient client(espClient);

byte uuidNumber[16]; // UUIDs in binary form are 16 bytes long
String uuid_buf;     // UUID in string byte
char uuid_name[37];  // UUID in char array type

long lastMsg = 0;
const char* relay_state = "OFF";

char buf_pub_topic[50];
char buf_sub_topic[50];

void setup() {
  pinMode(PIN_relay_switch, OUTPUT);
  pinMode(PIN_pwm_out, OUTPUT);
  pinMode(PIN_powerOn_led, OUTPUT);
  digitalWrite(PIN_powerOn_led, LOW);
  pinMode(AnalogPin, INPUT);

  analogWriteFreq(500);///////////////////////////////////////////////////////
  //analogWriteRange(new_range);/////////////////////////////////////////////////
  //analogWrite(PIN_pwm_out, 128);

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  EEPROM.begin(512);  // store values of relay state and brightness level setting

  ESP8266TrueRandom.uuid(uuidNumber);
  Serial.print("The UUID number is ");
  printUuid(uuidNumber);
  Serial.println();
  uuid_buf = ESP8266TrueRandom.uuidToString(uuidNumber);
  uuid_buf.toCharArray(uuid_name, 37);
  Serial.println(uuid_name);

  //brightness = 0; // set initial brightness
  brightness_poten = 0;
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");

  // Check topic of relay setting, and than control powerOn led, relay switch by result.
  set_sub_topic(relay_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      relay_state = "ON";
      Serial.println("Turning RELAY of Dimmer circuit open ON...");
      digitalWrite(PIN_powerOn_led, HIGH);
      digitalWrite(PIN_relay_switch, HIGH);
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {
      relay_state = "OFF";
      Serial.println("Turning RELAY of Dimmer circuit open OFF...");
      digitalWrite(PIN_powerOn_led, LOW);
      digitalWrite(PIN_relay_switch, LOW);
      analogWrite(PIN_pwm_out, 0);
    }
  }
  // Check topic of brightness level setting, and than control Dimmer circuit by result.
  else {
    set_sub_topic(brightness_level_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      val = (char*)payload;

      /*for (int i = 0; i < length; i++) {
        brightness_level[i] = (char)payload[i];
      }*/

      curr_brightness_level = atoi(val);

      //Serial.print("Current brightness level is ");//////////////////////////
      //Serial.println(curr_brightness_level);////////////////////////////////////////

      SetBrightness(&curr_brightness_level);
      if (relay_state == "ON") {
        Serial.println("Brightness will be controled by mobile app...");
        //analogWrite(PIN_pwm_out, brightness);
        //delay(500);  //????
      }
    }
  }
  EEPROM_Write();
}

void printHex(byte number) {
  int topDigit = number >> 4;
  int bottomDigit = number & 0x0f;
  // Print high hex digit
  Serial.print( "0123456789ABCDEF"[topDigit] );
  // Low hex digit
  Serial.print( "0123456789ABCDEF"[bottomDigit] );
}

void printUuid(byte* uuidNumber) {
  int i;
  for (i = 0; i < 16; i++) {
    if (i == 4) Serial.print("-");
    if (i == 6) Serial.print("-");
    if (i == 8) Serial.print("-");
    if (i == 10) Serial.print("-");
    printHex(uuidNumber[i]);
  }
}

void EEPROM_Write(void) {

  byte val_state;
  byte hiByte, loByte;

  if (relay_state == "ON") {
    val_state = 1;
  }
  else if (relay_state == "OFF") {
    val_state = 0;
  }

  EEPROM.write(0, val_state);
  EEPROM.write(1, curr_brightness_level);
  hiByte = highByte(brightness);
  loByte = lowByte(brightness);
  EEPROM.write(2, hiByte);
  EEPROM.write(3, loByte);
  EEPROM.commit();
}

void EEPROM_Read(void) {

  byte val_state;
  byte hiByte, loByte;
  val_state = EEPROM.read(0);

  if (val_state == 1) {
    relay_state = "ON";
    digitalWrite(PIN_powerOn_led, HIGH);
    digitalWrite(PIN_relay_switch, HIGH);
  }
  else if (val_state == 0) {
    relay_state = "OFF";
    digitalWrite(PIN_powerOn_led, LOW);
    digitalWrite(PIN_relay_switch, LOW);
  }

  curr_brightness_level = EEPROM.read(1);
  hiByte = EEPROM.read(2);
  loByte = EEPROM.read(3);
  brightness = word(hiByte, loByte);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(uuid_name)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe
      set_sub_topic(relay_set_suffix);
      client.subscribe(buf_sub_topic);
      set_sub_topic(brightness_level_set_suffix);
      client.subscribe(buf_sub_topic);
      // client.subscribe("common");

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void loop() {
  if (!client.connected()) {
    reconnect();

    EEPROM_Read();
    Serial.println("Setting values was updated from eeprom");
    Serial.print("relay state: ");
    Serial.println(relay_state);
    Serial.print("brightness_level set: ");
    Serial.println(curr_brightness_level);
  }
  client.loop();

  brightness_poten = analogRead(AnalogPin);
  //Serial.print(brightness_poten);
  brightness_poten_level = 20 - (brightness_poten / 50);

  //char* buf_brightness_poten_level = new char[10];
  dtostrf(brightness_poten_level, 3, 0, buf_brightness_poten_level);
  //char* buf_curr_brightness_level = new char[10];
  dtostrf(curr_brightness_level, 3, 0, buf_curr_brightness_level);

  Serial.println(buf_curr_brightness_level);
  
  Serial.print("potentiometer level: ");
  Serial.print(brightness_poten_level);
  Serial.print("( ");
  Serial.print(brightness_poten);
  Serial.println(" )");

  if (relay_state == "ON") {
    analogWrite(PIN_pwm_out, brightness);
    // output current brightness level through serial port
    Serial.print("setting level: ");
    Serial.print(curr_brightness_level);
    Serial.print("( ");
    Serial.print(brightness);
    Serial.println(" )");
  }
  // publish relay state
  set_pub_topic(relay_state_suffix);
  client.publish(buf_pub_topic, relay_state);

  //publish current brightness level
  set_pub_topic(curr_brightness_level_suffix);
  client.publish(buf_pub_topic, buf_curr_brightness_level);
  // publish brightness level by potentiometer
  set_pub_topic(brightness_poten_level_suffix);
  client.publish(buf_pub_topic, buf_brightness_poten_level);

  delay(1000);   // Loop function takes about 300ms, so 400 ms is enough.
}

void SetBrightness(int *curr_brightness_level) {    //set brightness value for PWM driver

  if (*curr_brightness_level > brightness_poten_level) {  // restrict brightness value for mobile app//////////////////////////////////
    *curr_brightness_level = brightness_poten_level;
  }
  Serial.print("Current brightness level is ");
  Serial.println(*curr_brightness_level);

  switch (*curr_brightness_level) {
    case 0: brightness = 1024; break;
    case 1: brightness = 950; break;
    case 2: brightness = 900; break;
    case 3: brightness = 850; break;
    case 4: brightness = 800; break;
    case 5: brightness = 750; break;
    case 6: brightness = 700; break;
    case 7: brightness = 650; break;
    case 8: brightness = 600; break;
    case 9: brightness = 550; break;
    case 10: brightness = 500; break;
    case 11: brightness = 450; break;
    case 12: brightness = 400; break;
    case 13: brightness = 350; break;
    case 14: brightness = 300; break;
    case 15: brightness = 250; break;
    case 16: brightness = 200; break;
    case 17: brightness = 150; break;
    case 18: brightness = 100; break;
    case 19: brightness = 50; break;
    case 20: brightness = 1; break;
    default:
      Serial.println("Invalid entry");
  }
  /*if (brightness > brightness_poten) {  // restrict brightness value for mobile app//////////////////////////////////
    brightness = brightness_poten;                                                     //////////////////////////////////////
  }*/
}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}


